variable "aws_region" {
  type    = string
  default = "ca-central-1"
}

variable "db_table_name" {
  type    = string
  default = "echan-terraform-ci-1"
}

variable "db_read_capacity" {
  type    = number
  default = 1
}

variable "db_write_capacity" {
  type    = number
  default = 1
}
